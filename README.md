# searchwing-pi-manager

![alt text](https://gitlab.com/searchwing/development/searchwing-pi-manager/-/raw/master/screenshot.jpg)

A tool to manage the pi cameras in multiple drones. It serves as a easy GUI for preflight system check. It uses SSH connections to gather the infos and visulizes it via a qt gui.

Features:
* Manage multiple drones by a list of ip addresses
* Enable / disable camera service
* Watch disk usage
* Edit params of the camera
* Debug camera service
* Fast showing of last photo of each drone in browser

## install
* Install pyqt: using `install_pyqt5.sh` script
* Install requirements: `pip3 install -r requirements.txt`

## config
use the config.yaml to configure your drone-connections.

## run
Run GUI:
```
python3 manager.py
```

## develop
Compile ui created with the qt designer:
```
pyuic5 manager.ui -o manager_ui.py 
```
