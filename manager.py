from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QFileDialog, QMessageBox
from PyQt5.QtCore import pyqtSignal, pyqtSlot

import manager_ui

import sys
import paramiko
import time
import os
import yaml
from functools import partial
import logging
from io import StringIO
import socket
import webbrowser

#read config
config_path = os.path.join(os.path.dirname(__file__), "config.yaml")
config_path = os.path.realpath(config_path)
if not os.path.exists(config_path):
    print("No config file specified!")
with open(config_path, "r") as f:
    config = yaml.safe_load(f)
    
class cameraGuiGroup():
    def __init__(self,ipTextbox,connectBtn,disconnectBtn, camServTextbox,camServStartBtn,camServStopBtn, 
    dataUsageBar, cmdlogTextEdit, camServiceTextEdit, camServStatusColor,connectedText, connectedColor, takeSingleImgBtn, viewLatestImgBtn,
    paramsTextEdit, readparamsMav, readparamsDisk, writeparamsMav ):
        self.ipTextbox=ipTextbox
        self.connectBtn=connectBtn
        self.disconnectBtn=disconnectBtn
        self.camServTextbox=camServTextbox
        self.camServStartBtn = camServStartBtn
        self.camServStopBtn = camServStopBtn

        self.dataUsageBar = dataUsageBar
        self.camServStatusColor = camServStatusColor
        self.cmdlogTextEdit = cmdlogTextEdit
        self.camServiceTextEdit = camServiceTextEdit
        self.connectedText = connectedText
        self.connectedColor = connectedColor
        self.takeSingleImgBtn = takeSingleImgBtn
        self.viewLatestImgBtn = viewLatestImgBtn

        self.paramsTextEdit = paramsTextEdit
        self.readparamsMav = readparamsMav
        self.readparamsDisk = readparamsDisk
        self.writeparamsMav = writeparamsMav

        rMyIcon = QtGui.QPixmap("camera-icon.png")
        self.takeSingleImgBtn.setIcon(QtGui.QIcon(rMyIcon))

class droneStateData(object):
    def __init__(self):
        self.connected = False
        self.servState = ""
        self.servLog = ""
        self.cmdLog = ""
        self.dataUsage = None

class sshConnection(QtCore.QThread):

    sigDroneState = pyqtSignal(int, droneStateData)
    sigReadFile = pyqtSignal(int, str)

    def __init__(self,guiid,ip,user,pw, timeout):
        super(sshConnection, self).__init__()
        self.guiid = guiid
        self.ip=ip
        self.user=user
        self.pw=pw
        
        self.ssh = None

        self.logger = logging.getLogger(ip)

        if config['logLevel'] == "INFO":
            self.logger.setLevel(logging.INFO)
        elif config['logLevel'] == "ERROR":
            self.logger.setLevel(logging.ERROR)
        elif config['logLevel'] == "DEBUG":
            self.logger.setLevel(logging.DEBUG)


        formatter = logging.Formatter("[%(asctime)s]-[%(levelname)s]:\t%(message)s","%Y-%m-%d %H:%M:%S")

        self.log_stream = StringIO()    
        self.logConsoleOut = logging.StreamHandler(self.log_stream)
        self.logConsoleOut.setFormatter(formatter)
        self.logger.addHandler(self.logConsoleOut)
        self.timeout=timeout

        self.connected = False

    def __del__(self):
        if not self.ssh is None:
            self.ssh.close()

    def log_std(self,stdout,stderr):
        for oneOut in stdout:
            self.logger.info(oneOut)
        for oneErr in stderr:
            self.logger.error(oneErr)

    def connect(self):
        if self.connected == True:
            return

        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy()) 
        
        self.logger.info("Connect to {}@{}".format(self.user,self.ip))
        try:
            self.ssh.connect(self.ip, username=self.user, password=self.pw, timeout=self.timeout)
        except paramiko.SSHException as e:
            self.logger.error(e)
            return
        except Exception as e:
            self.logger.error(e)
            return

        self.connected = True
        self.logger.info("Done!")

    def disconnect(self):
        if not self.ssh is None:
            self.ssh.close()
            self.connected = False

    def kill(self):
        self.disconnect()
        self.terminate()

    def reset(self):
        self.connected = False
        self.ssh.close()

    def exec_cmd_sudo(self,cmd):
        self.logger.debug(cmd)
        if self.connected == False:
            return
        try:
            stdin, stdout, stderr = self.ssh.exec_command("sudo -S "+cmd, get_pty=True, timeout=self.timeout)
            stdin.write(self.pw + "\n")
            stdin.flush()
        except Exception as e:
            self.connected = False
            self.logger.error(e)
            return "","",""
        return stdin, stdout, stderr

    def exec_cmd(self,cmd):
        self.logger.debug(cmd)
        if self.connected == False:
            return
        try:
            stdin, stdout, stderr = self.ssh.exec_command(cmd, get_pty=False, timeout=self.timeout)
        except Exception as e:
            self.connected = False
            self.logger.error(e)
            return
        return stdin, stdout, stderr
    
    def set_service(self,name,value):
        if value > 0:
            set_val = "start"
        else:
            set_val = "stop"
        cmd = 'systemctl '+set_val+' '+name
        try:
            stdin, stdout, stderr = self.exec_cmd_sudo(cmd)
            out = stdout.readlines()[1:]
            if out != []:
                self.logger.info(out)
            err = stderr.readlines()[1:]
            if err != []:
                self.logger.error(err)
        except Exception:
            return ""

    def get_status_service(self,name):
        cmd = 'systemctl is-active '+name
        try:
            stdin, stdout, stderr = self.exec_cmd(cmd)
            status = stdout.readline()[:-1]
            return status
        except Exception:
            return ""

    def get_full_status_service(self,name):
        cmd = 'systemctl status '+name
        try:
            stdin, stdout, stderr = self.exec_cmd(cmd)
            full_status = ""
            for line in stdout:
                full_status = full_status + line
            return full_status
        except Exception:
            return ""

    def get_data_folder_size(self):
        cmd = "df -h /data"
        try:
            stdin, stdout, stderr = self.exec_cmd(cmd)
            text = stdout.readline()
            text = stdout.readline()
            if text != '':
                texts = text.split()
                used = texts[2]
                size = texts[1]
                perc = int(texts[4][:-1])
                return used, size, perc
        except Exception as e:
            return "","",0

    def get_status_log(self):
        full_status = self.get_full_status_service(config['camCaptureServName'])
        return full_status

    def get_status_label(self):
        status_label = self.get_status_service(config['camCaptureServName'])
        return status_label

    def get_logging(self):
        return self.log_stream.getvalue()

    def take_single_img(self):
        trigger_img_filepath = os.path.join(os.path.dirname(config['configFilePath']),"TAKE_IMAGE_TRIGGER")
        cmd = "touch " + trigger_img_filepath
        try:
            stdin, stdout, stderr = self.exec_cmd(cmd)
            self.logger.info("[CAMERA] Image trigger sent!".format(path))
        except Exception as e:
            return "","",0

    def update(self):
        state = droneStateData()
        state.connected = self.connected
        state.servState = self.get_status_label()
        state.servLog = self.get_status_log()
        state.dataUsage = self.get_data_folder_size()
        state.cmdLog = self.get_logging()

        self.sigDroneState.emit(self.guiid, state)

    @pyqtSlot(int, str, str)
    def write_file(self, id, path, paramsWrite):
        if id != self.guiid:
            return

        cmd = "echo '{}' > {}".format(paramsWrite, path)
        try: 
            stdin, stdout, stderr = self.exec_cmd(cmd)
        except Exception as e:
            return ""
        err = stderr.readlines()
        if err == []:
            #check if write is valid
            paramsRead = self.read_file(path)
            paramsRead = paramsRead[:-1] # removing trailing \n from read content
            if paramsRead == paramsWrite :
                self.logger.info("[PARAMS] Filewrite to {} successful!".format(path))
        else:
            self.logger.error("[PARAMS] Error while writing to {}!".format(path))
            self.logger.error(err)

    def read_file(self, path):
        cmd = "cat {}".format(path)
        fileContent = ""
        try: 
            stdin, stdout, stderr = self.exec_cmd(cmd)
            for line in stdout:
                fileContent = fileContent + line
            err = stderr.readlines()
            if err == []:
                self.logger.info("[PARAMS] Fileread from {} successful!".format(path))
            else:
                self.logger.error("[PARAMS] Error while reading from {}!".format(path))
                self.logger.error(err)
        except Exception as e:
            return ""
        return fileContent

    def send_read_file(self, path):
        fileContent = self.read_file(path)
        self.sigReadFile.emit(self.guiid, fileContent)

class ExampleApp(QtWidgets.QMainWindow, manager_ui.Ui_MainWindow):

    sigWriteFile = pyqtSignal(int, str, str)

    def __init__(self, parent=None):
        super(ExampleApp, self).__init__(parent)
        self.setupUi(self)

        self.camConns = []

        ########
        #Setup drone names
        ########
        self.droneNames = []
        for key, val in config['drones'].items() :     
            droneName = key
            self.droneNames.append(droneName)
            self.droneNamesCBox.addItem(droneName)

        self.droneNamesCBox.currentIndexChanged.connect(self.droneNameChange)
        self.curDroneName = ""
        self.cam1 = None
        self.cam2 = None
        
        ########
        #Setup GUI Objects
        ########
        self.cameraGuiGroups = [] 
        cameraGui1 = cameraGuiGroup(self.ipTextbox1, self.connectBtn1, self.DisconnectBtn1, \
            self.camServStatusTextbox1, self.StartCamServBtn1,self.StopCamServBtn1,self.dataUsageBar1, \
            self.cmdLog1, self.servLogOut1, self.CamServStatusColor1, \
            self.connectedText1, self.connectedColor1, self.takeSingleImgBtn1, self.viewLatest1, \
            self.paramsTextEdit1, self.readParamsMAV1, self.readParamsDisk1, self.writeParamsMAV1)
        cameraGui2 = cameraGuiGroup(self.ipTextbox2, self.connectBtn2, self.DisconnectBtn2, \
            self.camServStatusTextbox2, self.StartCamServBtn2,self.StopCamServBtn2,self.dataUsageBar2, \
            self.cmdLog2, self.servLogOut2, self.CamServStatusColor2, \
            self.connectedText2, self.connectedColor2, self.takeSingleImgBtn2, self.viewLatest2, \
            self.paramsTextEdit2, self.readParamsMAV2, self.readParamsDisk2, self.writeParamsMAV2)

        self.cameraGuiGroups.append(cameraGui1)
        self.cameraGuiGroups.append(cameraGui2)

        self.updatetimer = QtCore.QTimer(self)
        self.updatetimer.setInterval(config['refresh'])
        self.updatetimer.start()

        self.droneNameChange()

    def droneNameChange(self):   
        #disconnect all old cam connections
        try: self.updatetimer.timeout.disconnect()
        except Exception: pass
        
        #delete old thread / connections
        for oneCamConn in self.camConns:
            oneCamConn.kill()
            oneCamConn.quit()
            oneCamConn.wait()
        self.camConns.clear()

        #create new connection information
        self.curDroneName = self.droneNamesCBox.currentText()

        if not self.cam1 is None:
            self.cam1.__del__()
        if not self.cam2 is None:
            self.cam2.__del__()

        droneCfg = config['drones'][self.curDroneName]
        ip1 = droneCfg[0]['ip1']
        ip2 = droneCfg[1]['ip2']

        #create new connections 
        if ip1 != "":
            self.cameraGuiGroups[0].ipTextbox.setText(ip1)
            self.addCamConn(0,ip1,droneCfg[2]['user1'],droneCfg[4]['pw1'])
        if ip2 != "":
            self.cameraGuiGroups[1].ipTextbox.setText(ip2)
            self.addCamConn(1,ip2,droneCfg[3]['user2'],droneCfg[5]['pw2'])

    def addCamConn(self, id, ip, user, pw):
            camConn = sshConnection(id, ip, user, pw, config['SSHtimeout']) 
            #connection
            self.cameraGuiGroups[id].connectBtn.clicked.connect(camConn.connect)
            self.cameraGuiGroups[id].disconnectBtn.clicked.connect(camConn.disconnect)
            self.connectBothBtn.clicked.connect(camConn.connect)      
            self.updatetimer.timeout.connect(camConn.update)

            #cam service
            self.cameraGuiGroups[id].camServStartBtn.clicked.connect(partial(camConn.set_service,config['camCaptureServName'], True))
            self.cameraGuiGroups[id].camServStopBtn.clicked.connect(partial(camConn.set_service,config['camCaptureServName'], False))
            self.cameraGuiGroups[id].takeSingleImgBtn.clicked.connect(camConn.take_single_img)
            self.cameraGuiGroups[id].viewLatestImgBtn.clicked.connect(partial(self.viewLatestImg,ip))
            camConn.sigDroneState.connect(self.visualizeDroneState)

            #params
            #write
            self.cameraGuiGroups[id].writeparamsMav.clicked.connect(partial(self.writeParamsToMav, id))
            self.sigWriteFile.connect(camConn.write_file)
            #read
            camConn.sigReadFile.connect(self.visParams)
            self.cameraGuiGroups[id].readparamsMav.clicked.connect(partial(camConn.send_read_file,config['configFilePath']))
            self.cameraGuiGroups[id].readparamsDisk.clicked.connect(partial(self.readParamsFromDisk,id))

            camConn.start()
            self.camConns.append(camConn)

    def visualizeDroneState(self, guiId:int, data:droneStateData):
        gui = self.cameraGuiGroups[guiId]

        #connected
        sshConncted = data.connected
        if sshConncted:
            gui.connectedText.setText("connected")
            gui.connectedColor.setStyleSheet("background-color:#00ff00")
        else:
            gui.connectedText.setText("disconnected")
            gui.connectedColor.setStyleSheet("background-color:#ff0000")

        #log texts
        gui.cmdlogTextEdit.setText(data.cmdLog)
        gui.camServiceTextEdit.setText(data.servLog)
        if self.autoScrollCheckBox.isChecked():
            gui.cmdlogTextEdit.moveCursor(QtGui.QTextCursor.End)
            gui.camServiceTextEdit.moveCursor(QtGui.QTextCursor.End)

        #serv status
        camServStatus = data.servState
        gui.camServTextbox.setText(camServStatus)
        if camServStatus == "active":
            gui.camServStatusColor.setStyleSheet("background-color:#00ff00")
        else:
            gui.camServStatusColor.setStyleSheet("background-color:#ff0000")     

        #data usage 
        if data.dataUsage is not None:
            used, size, perc = data.dataUsage
            gui.dataUsageBar.setValue(perc)
            gui.dataUsageBar.setFormat(used+"/"+size)

    def viewLatestImg(self,ip):
        webbrowser.open("http://" + ip + '/0_latest.jpg')

    def writeParamsToMav(self, id):
        path=config['configFilePath']
        paramsText=self.cameraGuiGroups[id].paramsTextEdit.toPlainText()
        self.sigWriteFile.emit(id, path, paramsText)

    def readParamsFromDisk(self, id):
        cfgPath = QFileDialog.getOpenFileName(self, 'Open Config file')
        try:
            with open(cfgPath[0], "r") as f:
                params =f.read()
                self.cameraGuiGroups[id].paramsTextEdit.setText(params)
        except Exception as e:
            self.cameraGuiGroups[id].paramsTextEdit.setText("ERROR opening Config-File!")

    def visParams(self, id, paramsText):
        if paramsText != "":
            self.cameraGuiGroups[id].paramsTextEdit.setText(paramsText)

def main():
    app = QApplication(sys.argv)
    form = ExampleApp()
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()
